#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Flask, request, g, abort, session, escape, Response, render_template, flash, url_for, redirect, make_response, jsonify
from utils import get_sha,  make_json_response, enable_only_in_dev, token_required

import jwt
import datetime
import os
import json
import requests
import logging


ENV_SECRET_KEY = os.environ["ENV_SECRET_KEY"]
IS_DEV = os.environ['IS_DEV']


app = Flask(__name__)
app.config.from_object(__name__)
app.secret_key = ENV_SECRET_KEY
app.config['IS_DEV'] = IS_DEV


logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(name)s %(levelname)-8s %(message)s')


@app.route('/', methods=['GET'])
@app.route('/index', methods=['GET'])
def index():
    try:
        return make_json_response(code=0, body=None, msg='Requestes endpoint /index')
    except Exception as e:
        return str(e)


@app.route('/only-dev', methods=['GET'])
@enable_only_in_dev
def only_dev():
    return make_json_response(msg='Susto o muerte')


@app.route('/unprotected')
def unprotected():
    return jsonify({'message' : 'Anyone can view this!'})

@app.route('/protected')
@token_required
def protected():
    return jsonify({'message' : 'This is only available for people with valid tokens.'})

@app.route('/login')
def login():
    auth = request.authorization

    if auth and auth.password == 'secret':
        token = jwt.encode({'user' : auth.username, 'exp' : datetime.datetime.utcnow() + datetime.timedelta(seconds=15000)}, app.config['SECRET_KEY'])

        return jsonify({'token' : token.decode('UTF-8')})

    return make_response('Could not verify!', 401, {'WWW-Authenticate' : 'Basic realm="Login Required"'})


if __name__ == '__main__':
    app.run('0.0.0.0', port=8080)
