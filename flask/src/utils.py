
from flask import session, url_for, redirect, jsonify
import time
import hashlib
import os


def get_sha(salt, length=40):
    """
    Retorna un hash de 40 caràcters a partir del timestamp actual (usecs).
    """
    hash = hashlib.sha1()
    hash.update((str(time.time())+salt).encode('utf-8'))
    return hash.hexdigest()[:length]


def enable_only_in_dev(f):
    def wrapper():
        IS_DEV = os.environ['IS_DEV']
        if IS_DEV != '1':
            return redirect(url_for('index'))
        else:
            return f()

    wrapper.__name__ = f.__name__
    return wrapper


def token_required(f):
    def wrapper(*args, **kwargs):
        token = request.args.get('token')

        if not token:
            return jsonify({'message' : 'Token is missing!'}), 403

        try: 
            data = jwt.decode(token, app.config['SECRET_KEY'])
        except Exception as e:
            print(str(e))
            return jsonify({'message' : 'Token is invalid!'}), 403

        return f(*args, **kwargs)

    return wrapper


def make_json_response(body=None, code=0, msg='ok'):
    status_code = 200
    return jsonify(code=code, message=msg, body=body), status_code

