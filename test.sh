#!/bin/bash

ab -T application/json -n 10000 -c 20 http://localhost:8080/index


# This is ApacheBench, Version 2.3 <$Revision: 1807734 $>
# Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
# Licensed to The Apache Software Foundation, http://www.apache.org/

# Benchmarking localhost (be patient)
# Completed 1000 requests
# Completed 2000 requests
# Completed 3000 requests
# Completed 4000 requests
# Completed 5000 requests
# Completed 6000 requests
# Completed 7000 requests
# Completed 8000 requests
# Completed 9000 requests
# Completed 10000 requests
# Finished 10000 requests


# Server Software:        Werkzeug/0.14.1
# Server Hostname:        localhost
# Server Port:            8080

# Document Path:          /index
# Document Length:        61 bytes

# Concurrency Level:      20
# Time taken for tests:   12.964 seconds
# Complete requests:      10000
# Failed requests:        0
# Total transferred:      2070000 bytes
# HTML transferred:       610000 bytes
# Requests per second:    771.39 [#/sec] (mean)
# Time per request:       25.927 [ms] (mean)
# Time per request:       1.296 [ms] (mean, across all concurrent requests)
# Transfer rate:          155.94 [Kbytes/sec] received

# Connection Times (ms)
#               min  mean[+/-sd] median   max
# Connect:        0    0   0.1      0       3
# Processing:     5   26   3.3     26     211
# Waiting:        3   25   3.2     24     210
# Total:          8   26   3.3     26     211

# Percentage of the requests served within a certain time (ms)
#   50%     26
#   66%     27
#   75%     28
#   80%     28
#   90%     29
#   95%     30
#   98%     32
#   99%     34
#  100%    211 (longest request)
